using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class StatusVenda
    {
        public int Id { get; set; }
        public string NomeStatus { get; set; }
    }
}