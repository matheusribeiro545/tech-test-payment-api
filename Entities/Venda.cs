using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        // public virtual Vendedor Vendedor { get; set; } // Chave estrangeira de acordo com o EF
        public int ProdutoId { get; set; }
        // public virtual Produto Produto { get; set; } // Chave estrangeira de acordo com o EF
        public int QuantidadeProduto { get; set; }
        public DateTime DataVenda { get; set; } // Chave estrangeira de acordo com o EF
        public int StatusVendaId { get; set; }
        // public virtual StatusVenda StatusVenda { get; set; }
        public DateTime DataUltimoStatus { get; set; }
    }
}