using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly EcommerceContext _context;

        public VendaController(EcommerceContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult NovaVenda(Venda venda)
        {   
            _context.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("ObterPorId")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

    }
}