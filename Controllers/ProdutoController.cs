using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly EcommerceContext _context;

        public ProdutoController(EcommerceContext context)
        {
            _context = context;
        }
        
        [HttpPost]
        public IActionResult CadastrarProduto(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();

            return Ok(produto);
        }
    	
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var produto = _context.Produtos.Find(id);

            if (produto == null)
                return NotFound();

            return Ok(produto);
        }
        
        [HttpGet("ObterPorNome")]
        public IActionResult ObterPorNome(string nomeProduto)
        {
            var produtos = _context.Produtos.Where(x => x.NomeProduto.Contains(nomeProduto));

            return Ok(produtos);
        }

        [HttpDelete("DeletarProduto/{id}")]
        public IActionResult DeletarProduto(int id)
        {
            var produtoBanco = _context.Produtos.Find(id);

            if (produtoBanco == null)
                return NotFound();

            _context.Produtos.Remove(produtoBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}