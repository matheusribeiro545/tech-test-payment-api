using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StatusVendaController : ControllerBase
    {
        private readonly EcommerceContext _context;

        public StatusVendaController(EcommerceContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var statusVenda = _context.StatusVenda.Find(id);

            if (statusVenda == null)
                return NotFound();

            return Ok(statusVenda);
        }

        [HttpGet("ObterPorNome")]
        public IActionResult ObterPorNome(string nome)
        {
            var statusVenda = _context.StatusVenda.Where(x => x.NomeStatus.Contains(nome));

            return Ok(statusVenda);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var statusVenda = _context.StatusVenda;

            return Ok(statusVenda);
        }
    }
}